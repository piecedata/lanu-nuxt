export default function ({ isDev, $axios }) {
  $axios.defaults.headers.common['X-Lanu'] = true;

  if (isDev) {
    $axios.defaults.headers.common['X-Lanu-Dev'] = true;
  }
}
