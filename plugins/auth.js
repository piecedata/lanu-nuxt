export default function ({ $axios, redirect, route, app: { localePath } }) {
  $axios.onError(({ response }) => {
    if (response && response.status === 401) {
      const url = {
        name: 'login',
        query: { next: route.fullPath },
      };

      if (localePath) {
        return redirect(localePath(url));
      }
      return redirect(url);
    }
  });
}
