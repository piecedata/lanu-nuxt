import Vue from 'vue';

// store/index.js
import app from './modules/app';
// get the options out using lodash templates
const options = JSON.parse(`<%= JSON.stringify(options) %>`);
// extract the namespace var
const { namespace } = options;
// create the plugin
export default (ctx, inject) => {
  const { store } = ctx;
  // Register module using namespace as the name.
  // module takes the options object and returns an object that is a
  // VueX store defenition
  store.registerModule(namespace, app(options), {
    // if the store module already exists, preserve it
    preserveState: Boolean(store.state[namespace]),
  });

  Vue.mixin({
    computed: {
      $app() {
        return this.$store.state[namespace].data;
      },

      $auth() {
        return this.$store.state[namespace].data.auth;
      },

      $page() {
        return this.$store.state[namespace].data.props;
      },
    },
  });
}
