import Middleware from '../../middleware';

const options = JSON.parse(`<%= JSON.stringify(options) %>`);
const { namespace } = options;

let i = 0;
let data = null;

Middleware[namespace] = async ctx => {
  const { isDev, app, store, route, error, $axios, redirect, next } = ctx;

  data = null;

  // check initial state and only for first request
  if (i === 0 && window.__INITIAL_STATE__) {
    data = window.__INITIAL_STATE__;
  }

  // fetch data
  try {
    if (!data) {
      const response = await $axios.$get(route.fullPath);

      if (response.redirect) {
        const external = response.redirect.indexOf('http') === 0;

        if (external) {
          next(false);
          window.location.href = response.redirect;
          return;
        } else {
          return redirect(response.redirect);
        }
      }

      data = response;
    }

    // set csrfToken
    $axios.defaults.headers.common['X-CSRF-TOKEN'] = data.csrfToken;

    // update document title but...
    app.head.title = data.title;
    document.title = data.title;

    if (i === 0) {
      app.router.app.$watch('$route', () => {
        store.dispatch(`${namespace}/set`, data);
      });
      store.dispatch(`${namespace}/set`, data);
      i++;
    }
  } catch ({ response }) {
    if (response) {
      const { status: statusCode, statusText: message } = response;

      return error({
        statusCode,
        message,
      });
    }

    return error('Error');
  }
};
